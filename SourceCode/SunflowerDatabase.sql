SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

CREATE DATABASE IF NOT EXISTS `SunflowerDatabase` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `SunflowerDatabase`;

CREATE TABLE `Account` (
  `Id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `Username` VARCHAR(20) NOT NULL,
  `Password` VARCHAR (128),
  `Role` VARCHAR(10),
  `Fullname` VARCHAR(128) COLLATE utf8_unicode_ci NOT NULL,
  `Email` VARCHAR(128) NOT NULL,
  `AvatarURL` VARCHAR(512) DEFAULT NULL,
  `DateOfBirth` DATE DEFAULT NULL
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

INSERT INTO `Account` (`Username`, `Password`, `Role`, `Fullname`, `Email`, `AvatarURL`, `DateOfBirth`)
VALUES
('trang', '25f9e794323b453885f5181f1b624d0b', 'Student', 'Hoàng Thị Thùy Trang', 'trang@gmail.com', '/App/View/Source/Image/Student/avatar-default.png', '2001-02-19'),
('luuquangthang', '25f9e794323b453885f5181f1b624d0b', 'Student', 'Lưu Quang Thắng', 'luuquangthang@gmail.com', '/App/View/Source/Image/Student/avatar-default.png', '2000-10-24'),
('admin', '25f9e794323b453885f5181f1b624d0b', 'Admin', 'Sunflower', 'sunflower@gmail.com', '', '2000-10-24'),
('thoa', '25f9e794323b453885f5181f1b624d0b', 'Teacher', 'Vương Kim Thoa', 'thoa@gmail.com', '/App/View/Source/Image/Teacher/VuongKimThoa.jpg', '1985-03-10'),
('tuan', '25f9e794323b453885f5181f1b624d0b', 'Teacher', 'Trần Anh Tuấn', 'tuan@gmail.com', '/App/View/Source/Image/Teacher/AnhTuan.jpg', '1984-03-3');

CREATE TABLE `AccountTeacher`(
  `Username` VARCHAR(20) NOT NULL,
  `Language` VARCHAR (20) NOT NULL,
  `PhoneNumber` VARCHAR(20) NOT NULL,
  `Degree` VARCHAR(512) NOT NULL,
  `Rating` FLOAT DEFAULT NULL
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;
INSERT INTO `AccountTeacher` (`Username`, `Language`, `PhoneNumber`, `Degree`, `Rating`)
VALUES
('thoa', 'Tiếng Anh', '0387504444', 'Đạt IELTS 8.5 với phần thi Speaking đạt 9.0 – điểm số tuyệt đối+ 5 năm kinh nghiệm dạy tiếng Anh giao tiếp và luyện IELTS tại Thái Lan, 3 năm kinh nghiệm luyện IELTS nâng cao tại Việt Nam', 4.8),
('tuan', 'Tiếng Trung', '0924896968', 'Đã có kinh nghiệm dạy học 2 năm ở Canada', 4.5);

CREATE TABLE `Course`(
  `Id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `CourseName` VARCHAR(256) NOT NULL,
  `CourseImageAddress` VARCHAR(512) DEFAULT NULL,
  `CourseDescription` VARCHAR(512) NOT NULL,
  `StartDateCourse` DATE NOT NULL,
  `EndDateCourse` DATE NOT NULL,
  `CourseTime` VARCHAR(100) NOT NULL,
  `CourseLanguage` VARCHAR(100) NOT NULL,
  `CoursePrice` INT NOT NULL,
  `CourseSlot` INT NOT NULL
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

INSERT INTO `Course` (`CourseName`, `CourseImageAddress`, `CourseDescription`, `StartDateCourse`, `EndDateCourse`, `CourseTime`, `CourseLanguage`, `CoursePrice`, `CourseSlot`)
VALUES
('Khóa học tiếng Anh cơ bản chỉ từ 840k/tháng', '/App/View/Source/Image/Course/course-speaking-english.jpg', 'Khóa học này mang đến cho bạn những trải nghiệm thực tế, gắn lý thuyết vào thực hành để giúp bạn cải thiện giao tiếp xã hội, cũng như hiểu và tận dụng được nguồn thông tin đa dạng trên internet.', '2021-6-1', '2021-8-3', 'Tối T3, T5, T7', 'Tiếng Anh', '840000', '35'),
('Khóa học tiếng Trung cho người mới bắt đầu', '/App/View/Source/Image/Course/course-chinese-beginner.jpg', 'Khóa học giúp học viên có những kiến thức cơ bản ban đầu về tiếng Trung, trang bị những ngữ pháp cơ bản và đặc biệt là lượng từ vựng vô cùng thực tế.', '2021-5-14', '2021-7-9', 'Sáng T2, T4, T6', 'Tiếng Trung', '650000', '35'),
('Khóa học tiếng Trung học thử miễn phí', '/App/View/Source/Image/Course/course-chinese-free-trial.png', 'Khóa học ra đời với mục đích giúp các bạn tiết kiệm được thời gian, công sức, tiền bạc, nhưng vẫn đảm bảo các bạn nắm vững và sử dụng tiếng Trung một cách thành thạo.', '2021-5-28', '2021-7-30', 'Chiều T2, T4, T6', 'Tiếng Trung', '800000', '25'),
('Khóa học giao tiếp tiếng Anh cho người mới bắt đầu', '/App/View/Source/Image/Course/course-beginner-speaking-english.jpg', 'Đến với lớp học này bạn sẽ được luyện tập kỹ năng giao tiếp tiếng Anh trực tiếp với giáo viên bản xứ.', '2021-5-28', '2021-7-21', 'Tối T2, T4, T6', 'Tiếng Anh', '950000', '35'),
('Tiếng Hàn phá tan mọi rào cản cho người mới bắt đầu', '/App/View/Source/Image/Course/course-korean-beginner-breaking.jpg', 'Khoá học này giúp bạn nắm chắc phần ngữ pháp thông dụng trong tiếng Hàn. Tích lũy được một lượng lớn một lượng lớn từ vựng thông dụng và cần thiết để sử dụng trong học tập, công việc và đời sống hàng ngày.', '2021-6-5', '2021-9-4', 'Sáng T3, T5, T7', 'Tiếng Hàn', '890000', '25'),
('Khóa học thành thạo tiếng Hàn cơ bản chỉ với 499k', '/App/View/Source/Image/Course/course-korean-fluent-499k.png', 'Khóa học này giúp bạn phát triển kỹ năng nghe - nói - phản xạ tốt bằng tiếng Hàn trong giao tiếp hàng ngày và tự tin giao tiếp bằng tiếng Hàn với người bản xứ. Đồng thời nâng cao cơ hội giành học bổng cao khi du học Hàn Quốc.', '2021-10-26', '2021-12-30', 'Chiều T3, T5, T7', 'Tiếng Hàn', '499000', '35'),
('Khóa học tiếng Hàn nâng cao chỉ với 899k', '/App/View/Source/Image/Course/course-korean-master.jpg', 'Khoá học này giúp bạn hoàn toàn tự tin đọc và hiểu được nội dung của tờ tạp chí tiếng Hàn. Hòa nhập nhanh chóng với cuộc sống tại Hàn Quốc, tự tin tham gia các hoạt động cùng người Hàn trong các trường Đại học.', '2021-6-29', '2021-8-28', 'Tối T3, T5, T7', 'Tiếng Hàn', '899000', '20'),
('Khóa học tiếng Trung cơ bản cho mọi người', '/App/View/Source/Image/Course/course-chinese-basic.jpg', 'Khoá học này giúp học viên phát triển cả 4 kỹ năng nghe - nói - đọc - viết. Kết hợp với những bài giảng học viên sẽ được biết thêm nhiều thông tin bổ ích về văn hóa, con người, ẩm thực đất nước Trung Hoa.', '2021-6-4', '2021-8-2', 'Sáng T2, T4, T6', 'Tiếng Trung', '599000', '35'),
('Khóa học tiếng Anh cam kết đầu ra IELTS 5.5 thần tốc', '/App/View/Source/Image/Course/course-english-ielts-5.5.jpg', 'Cam kết chỉ sau 01 khóa học, học viên đạt IELTS 5.5 theo kết quả thi tại các cơ sở chính thức của Hội đồng Anh (BC và IDP). Đào tạo miễn phí 01 năm nếu học viên không đạt kết quả thi như trong cam kết.', '2021-6-1', '2021-8-28', 'Tối T3, T5, T7', 'Tiếng Anh', '699000', '30'),
('Khóa học tiếng Anh lấy chứng chỉ TOIEC từ 450 - 700+', '/App/View/Source/Image/Course/course-english-toeic-400-700.jpg', 'Khóa học giúp học viên nắm chắc các chủ đề ngữ pháp, vốn từ vựng cần thiết trong kỳ thi TOEIC. Nắm vững format và nắm được phương pháp nâng điểm cực nhanh trong thời gian ôn thi TOEIC ngắn.', '2021-6-7', '2021-8-27', 'Chiều T2, T4, T6', 'Tiếng Anh', '599000', '30'),
('Khóa học luyện thi tiếng Nhật lấy chứng chỉ N4', '/App/View/Source/Image/Course/course-japanese-n4.png', 'Mang lại cơ hội cho học viên chủ động giao tiếp với giảng viên và các bạn khác trong lớp kết hợp với sử dụng slide, flash card, tranh ảnh, các video trực quan nhằm giúp học viên tiếp thu bài nhanh chóng ngay tại lớp.', '2021-6-7', '2021-8-27', 'Sáng T2, T4, T6', 'Tiếng Nhật', '999000', '35'),
('Khóa học tiếng Nhật để lấy chứng chỉ N3 cấp tốc', '/App/View/Source/Image/Course/course-japanese-n3.jpg', 'Khóa học giúp cho bạn thành thạo 4 kỹ năng: Nghe - nói - đọc - viết. Bên cạnh đó, học viên có thể giao tiếp bằng tiếng Nhật các chủ đề thông dụng trong cuộc sống hoặc tham gia phỏng vấn du học với các trường Nhật Bản.', '2021-6-1', '2021-8-3', 'Chiều T3, T5, T7', 'Tiếng Nhật', '799000', '35'),
('Khai giảng khóa học "Tiếng Nhật là chuyện nhỏ"', '/App/View/Source/Image/Course/course-japanese-is-easy.jpg', 'Khóa học đem lại cho bạn kiến thức tổng quan mức độ sơ cấp để có thể tham dự kỳ thi JLPT đạt kết quả cao chỉ trong thời gian ngắn và tự tin nghe nói thông thạo như người bản xứ ở trình độ sơ cấp trong các tình huống giao tiếp hằng ngày.', '2021-6-4', '2021-8-2', 'Tối T2, T4, T6', 'Tiếng Nhật', '500000', '30');

CREATE TABLE `StudentCourse`(
  `IdStudent` INT NOT NULL,
  `IdCourse` INT NOT NULL
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;
INSERT INTO `StudentCourse` (`IdStudent`, `IdCourse`)
VALUES
('1', '1'),
('2', '1');


CREATE TABLE `TeacherCourse`(
  `IdTeacher` INT NOT NULL,
  `IdCourse` INT NOT NULL
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;
INSERT INTO `TeacherCourse` (`IdTeacher`, `IdCourse`)
VALUES
('4', '1'),
('4', '3');


CREATE TABLE `News`(
  `Id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `NewsTitle` VARCHAR(256) NOT NULL,
  `NewsImageAddress` VARCHAR(512) DEFAULT NULL,
  `NewsDescription` VARCHAR(1024) NOT NULL,
  `NewsContent` TEXT NOT NULL,
  `NewsReleaseDate` DATE DEFAULT CURDATE(),
  `NewsCategory`  VARCHAR(256) NOT NULL
)
ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_vietnamese_ci;

INSERT INTO `News` (`NewsTitle`, `NewsImageAddress`, `NewsDescription`, `NewsContent`, `NewsCategory`)
VALUES
('24 Idioms chinh phục 7.0 IELTS Speaking', '/App/View/Source/Image/News/english-idioms.png', 'Chắc hẳn các bạn đều biết sử dụng Idioms (thành ngữ) trong IELTS speaking sẽ giúp cho bạn có những điểm cộng trong phần thi này, tuy nhiên nếu sử dụng idioms không đúng trong ngữ cảnh bạn sẽ bị giám khảo trừ điểm.',
 '<p><strong>1. Don&rsquo;t count your chickens until they are hatched.</strong></p>

<p>Giải nghĩa: You should not make plans that depend on something good happening, because it might not (kh&ocirc;ng n&ecirc;n lập kế hoạch v&igrave; nghĩ rằng</p>

<p>mọi việc sẽ tốt đẹp trong khi những việc đ&oacute; vẫn chưa xảy ra; đừng n&oacute;i trước kẻo bước kh&ocirc;ng qua; đừng đếm cua trong lỗ).</p>

<p>V&iacute; dụ:</p>

<p>I wouldn&#39;t count your chickens, Mr Vass. I&#39;ve agreed to sign the contract, but that&#39;s all.</p>

<p>I think I&rsquo;m going to get straight A&rsquo;s on my tests, but I don&rsquo;t want to count my chickens before they&rsquo;re hatched.</p>

<p><strong>2. A bad/rotten apple</strong></p>

<p>Giải nghĩa: One bad person who has a bad effect on all the others in a group (th&agrave;nh phần c&aacute; biệt; con s&acirc;u l&agrave;m rầu nồi canh).</p>

<p>V&iacute; dụ:</p>

<p>There&rsquo;s always one bad apple in every class.</p>

<p>What do you plan to do about the bad apples in the company when you become CEO?</p>

<p><strong>3. Have egg on your face</strong></p>

<p>Giải nghĩa: To be made to look stupid by something embarrassing (trở th&agrave;nh tr&ograve; cười)</p>

<p>V&iacute; dụ:</p>

<p>The president was left with egg on his face after he called the ambassador by the wrong name.</p>

<p>Mike always said he could speak French fluently, but when he couldn&rsquo;t hold a conversation with the French tourist, he had egg all over his face.</p>

<p><strong>4. Burn the candle at both ends</strong></p>

<p>Giải nghĩa: To get very tired by doing things until very late at night and getting up early in the mornings (mệt mỏi v&igrave; phải thức khuya dậy sớm).</p>

<p>V&iacute; dụ:</p>

<p>If he doesn&rsquo;t stop burning the candle at both ends, he will end up in hospital.</p>

<p>I&rsquo;ve been burning the candle at both ends for too long. I need to slow down.</p>

<p><strong>5. Kill two birds with one stone</strong></p>

<p>Giải nghĩa: To achieve two things with one action (một mũi t&ecirc;n tr&uacute;ng hai đ&iacute;ch; nhất cử lưỡng tiện)</p>

<p>V&iacute; dụ:</p>

<p>I kill two birds with one stone by reading the news in English. I learn about current events AND improve my English.</p>

<p>You can kill two birds with one stone by brushing your teeth in the shower.</p>

<p><strong>6. Paint the town red</strong></p>

<p>Giải nghĩa: To go out to bars, clubs, etc. to enjoy yourself (&ldquo;quậy&rdquo; hết m&igrave;nh ở qu&aacute;n bar, c&acirc;u lạc bộ, v.v...để tận hưởng cuộc sống).</p>

<p>V&iacute; dụ:</p>

<p>Some people like nothing better than to paint the town red at every opportunity, but I prefer to stay at home and read books.</p>

<p>You got a bonus? We must paint the town red immediately!</p>

<p><strong>7. Bite off more than you can chew</strong></p>

<p>Giải nghĩa: To try to do more than you are able to do (&ocirc;m đồm nhiều việc vượt qu&aacute; khả năng của m&igrave;nh; tham thực cực th&acirc;n)</p>

<p>V&iacute; dụ:</p>

<p>At first I was really excited about my new project, but now I feel like I&rsquo;ve bitten off more than I can chew.</p>

<p>She bit off more than she could chew and quickly fell behind in her work.</p>

<p><strong>8. A storm in a teacup</strong></p>

<p>Giải nghĩa: An unnecessary expression of strong feelings about something that is very unimportant (ph&oacute;ng đại sự việc; chuyện b&eacute; x&eacute; ra to).</p>

<p>V&iacute; dụ:</p>

<p>The media is going crazy about it but if you ask me, it&rsquo;s just a storm in a teacup.</p>

<p>We will be alright. It seems bad now but we&rsquo;ll look back and see it was just a storm in a teacup.<br />
<br />
<strong>9. Bite the bullet</strong></p>

<p>Giải nghĩa: to accept something unpleasant because you cannot avoid it any longer. (chấp nhận một việc kh&ocirc;ng mấy dễ chịu v&igrave; kh&ocirc;ng thể tr&aacute;nh được nữa; cắn răng chịu đựng)</p>

<p>V&iacute; dụ:</p>

<p>I don&rsquo;t want to do military service but it&rsquo;s mandatory so I just have to bite the bullet.</p>

<p>I hate dentists but I have a toothache so it looks like I&rsquo;m going to have to bite the bullet.</p>

<p><strong>10. Cry over spilt milk</strong></p>

<p>Giải nghĩa: To waste time feeling sorry about an earlier mistake or problem that cannot be changed (tốn thời gian đi nuối tiếc những việc đ&atilde; xảy ra kh&ocirc;ng thay đổi được; kh&oacute;c g&agrave; quạ tha)</p>

<p>V&iacute; dụ:</p>

<p>It&rsquo;s broken but there&rsquo;s no use crying over spilt milk. We&rsquo;ll just have to buy a new one.</p>

<p>It&rsquo;s no use crying over spilt milk. Just forget it and try to do better next time.</p>

<p><strong>11. Dodge a bullet</strong></p>

<p>Giải nghĩa: to have a narrow escape from a dangerous situation (tho&aacute;t hiểm trong gang tấc)</p>

<p>V&iacute; dụ:</p>

<p>I wasn&rsquo;t wearing my helmet but the police officer didn&rsquo;t even notice. I really dodged a bullet!</p>

<p>The bus I missed had an accident. I feel like I dodged a bullet.<br />
<br />
<strong>12. Every cloud has a silver lining</strong></p>

<p>Giải nghĩa: There is something good even in a situation that seems very sad or difficult (trong c&aacute;i rủi c&oacute; c&aacute;i may, c&aacute;i kh&oacute; l&oacute; c&aacute;i kh&ocirc;n).</p>

<p>V&iacute; dụ:</p>

<p>Every cloud has a silver lining. Maybe losing your job will allow you to pursue your real dreams!</p>

<p>I broke my leg but then met my future wife in hospital. It&rsquo;s true that every cloud has a silver lining.<br />
<br />
<strong>13. A little bird told me</strong></p>

<p>Giải nghĩa: If you have information, but you do not want to reveal who told you it, you can say &#39;A little bird told me&rsquo; (khi bạn biết một điều g&igrave; đ&oacute; nhưng lại kh&ocirc;ng muốn tiết lộ nguồn tin của m&igrave;nh, bạn sẽ n&oacute;i &ldquo;A little bird told me&rdquo;).</p>

<p>V&iacute; dụ:</p>

<p>Who told you that she was up for a big promotion? A little bird told me.</p>

<p>I don&rsquo;t want to be rude but a little bird told me your girlfriend has been cheating on you.<br />
<br />
<strong>14. Have a green thumb</strong></p>

<p>Giải nghĩa: to be good at making plants grow. (m&aacute;t tay trồng c&acirc;y)</p>

<p>V&iacute; dụ:</p>

<p>Look at Sarah&rsquo;s garden! She really has a green thumb.</p>

<p>I admire people with green thumbs. Any plants entrusted to my care die quickly.</p>

<p><strong>15. Money talks</strong></p>

<p>Giải nghĩa: money is power and it can buy influence. (c&oacute; tiền mua ti&ecirc;n cũng được)</p>

<p>V&iacute; dụ:</p>

<p>You want to know how I did it? Money talks, my friend.</p>

<p>She&rsquo;s rich and money talks, so she always gets what she wants.</p>

<p><strong>16. Mind-blowing</strong></p>

<p>Giải nghĩa: absolutely amazing, astounding or shocking. (tuyệt vời, g&acirc;y ấn tượng mạnh, g&acirc;y shock)</p>

<p>V&iacute; dụ:</p>

<p>The computer game has mind-blowing graphics.</p>

<p>It is a mind-blowing experience to me. I will never forget it.<br />
<br />
<strong>17. Pay through the nose</strong></p>

<p>Giải nghĩa: to pay much more for something than it is really worth. (phải trả một c&aacute;i gi&aacute; cắt cổ)</p>

<p>V&iacute; dụ:</p>

<p>After the environmental disaster, the oil company had to pay through the nose to clean it all up.</p>

<p>Be careful your car doens&rsquo;t break down out in the country. The local mechanics will make you pay through the nose to fix it.</p>

<p><strong>18. Shoot yourself in the foot</strong></p>

<p>Giải nghĩa: To say or do something stupid that will cause you a lot of trouble. (n&oacute;i hay l&agrave;m việc xuẩn ngốc v&agrave; tự g&acirc;y rắc rối cho ch&iacute;nh m&igrave;nh; tự m&igrave;nh hại m&igrave;nh)</p>

<p>V&iacute; dụ:</p>

<p>If he keeps talking, pretty soon he&#39;ll shoot himself in the foot.</p>

<p>The applicant shot himself in the foot when he mentioned how many other people were interested in the job.<br />
<br />
<strong>19. Turn green with...</strong></p>

<p>Giải nghĩa: to feel a lof of jealousy with what someone else has. (cảm thấy v&ocirc; c&ugrave;ng ghen tị với những g&igrave; người kh&aacute;c c&oacute; m&agrave; m&igrave;nh kh&ocirc;ng c&oacute;; ghen tức xanh mặt; ghen ăn tức ở)</p>

<p>V&iacute; dụ:</p>

<p>They turned green envy when my wife walked in the room.</p>

<p>I was green with envy when I saw the rich man&rsquo;s collection of sports cars.<br />
<br />
<strong>20. The sky is the limit</strong></p>

<p>Giải nghĩa: There is no limit to what can be achieved. (kh&ocirc;ng c&oacute; giới hạn cho những th&agrave;nh tựu m&agrave; ai đ&oacute; c&oacute; thể đạt được)</p>

<p>V&iacute; dụ:</p>

<p>These days it seems the sky is the limit for successful young Internet ventures.</p>

<p>You can do anything you put your mind to. The sky is the limit.</p>

<p><strong>21. Wash your hands of something</strong></p>

<p>Giải nghĩa: to refuse to be responsible for something any more. (từ chối tiếp tục chịu tr&aacute;ch nhiệm cho một việc n&agrave;o đ&oacute;)</p>

<p>V&iacute; dụ:</p>

<p>The CEO couldn&rsquo;t wait to wash his hands of the money-losing operation.</p>

<p>I washed my hands of the whole situation and I never want to hear about it again.<br />
<br />
<strong>22. Money doesn&rsquo;t grow on trees</strong></p>

<p>Giải nghĩa: used to say that you should not waste money (D&ugrave;ng để nhắc bạn kh&ocirc;ng n&ecirc;n phung ph&iacute; tiền bạc &ldquo;Tiền kh&ocirc;ng phải l&agrave; vỏ hến&rdquo;.)</p>

<p>V&iacute; dụ:</p>

<p>No, I won&rsquo;t buy you that toy robot. Money doesn&rsquo;t grow on trees, you know.</p>

<p>You should know that money doesn&rsquo;t grow on trees. You have to save money</p>

<p><strong>23. See red</strong></p>

<p>Giải nghĩa: To become very angry</p>

<p>(Nổi giận đỏ m&agrave;y đỏ mặt, &ldquo;Giận t&iacute;m mặt&rdquo;.)</p>

<p>V&iacute; dụ:</p>

<p>When he called me an idiot I saw red. I&rsquo;ve never been angrier.</p>

<p>The boss will see red if you make another mistake on this. Do something at gunpoint.</p>

<p><strong>24. Do something at gunpoint</strong></p>

<p>Giải nghĩa: To do something reluctantly, as a result of being strongly forced.&nbsp;(L&agrave;m việc g&igrave; đ&oacute; một c&aacute;ch miễn cưỡng do bị &eacute;p buộc.)</p>

<p>V&iacute; dụ:</p>

<p>I wouldn&rsquo;t sing in public unless I was at gunpoint.</p>

<p>She might go out on a date with you&hellip;at gunpoint! Haha!</p>

<p>C&aacute;c bạn lưu v&agrave;o v&agrave; học nh&eacute;!</p>
', 'Tài liệu');

INSERT INTO `News` (`NewsTitle`, `NewsImageAddress`, `NewsDescription`, `NewsContent`, `NewsCategory`)
VALUES
('Học bổng du học các nước và điều cần biết', '/App/View/Source/Image/News/study-abroad.jpg', 'Xin học bổng du học để đỡ phần chi phí khi theo học tại các trường nước ngoài là điều mà nhiều học sinh, sinh viên mong mỏi. Vậy có những trường nào, nước nào cung cấp các khóa học bổng ra sao? Điều kiện cần để xin học bổng là gì?',
 '<h2><strong>I. Th&ocirc;ng tin học bổng du học c&aacute;c nước</strong></h2>

<p>Mỗi nước v&agrave; trường c&oacute; c&aacute;c hạng mục học bổng kh&aacute;c nhau d&agrave;nh cho c&aacute;c bạn ngo&agrave;i nước. Học bổng từ bậc THCS, THPT hay ĐH, dự bị ĐH, cao đẳng, Thạc sĩ&hellip;đa dạng tại nhiều trường mở ra cơ hội xin học bổng nhiều hơn cho người Việt.</p>

<p>Dưới đ&acirc;y l&agrave; sơ lược một số học bổng kh&aacute;c nhau cho c&aacute;c bạn tham khảo.</p>

<p style="text-align:center"><img alt="học bổng du học và điều cần biết 1" src="https://static.ielts-fighter.com/uploads/2020/10/30/hoc-bong-du-hoc-va-cac-thong-tin-can-biet-1.jpg" style="height:466px; width:700px" /></p>

<p style="text-align:center"><em>Du học v&agrave; xin học bổng du học mỗi nước kh&aacute;c nhau</em></p>

<h3><strong>1. Học bổng du học Anh</strong></h3>

<p>Hiện c&aacute;c trường tại Anh c&oacute; học bổng từ 10-100% d&agrave;nh cho kh&oacute;a phổ th&ocirc;ng, dự bị đại học, đại học. Đặc biệt l&agrave; nhiều học bổng 1.000-5.000 GBP tại c&aacute;c trường ĐH như University of Greenwich, London South Bank University (LSBU),Coventry University,&nbsp; University of Huddersfield, University of Herdfordshire, Plymouth University, University of Portsmouth, Queen&rsquo;s University Belfast (QUB), University of East Anglia (UEA), Birmingham City University (BCU), UCLAN,</p>

<h3><strong>2. Học bổng du học &Uacute;c</strong></h3>

<p>Những trường đại học như Australian National University, Swinburne University,The University of Sydney, Queensland University of technology,&nbsp; Latrobe University, RMIT, University of ollongong, The University of Western Australia,&nbsp; University of technology Sydney, University of Canberra&hellip;đều c&oacute; học bổng từ 10-50% để c&aacute;c bạn học sinh, sinh vi&ecirc;n hướng tới.</p>

<h3><strong>3. Học bổng du học Mỹ</strong></h3>

<p>C&aacute;c trường ở Mỹ c&oacute; nhiều mức học bổng cao từ b&aacute;n phần đến to&agrave;n phần ở chương tr&igrave;nh THPT v&agrave; ĐH, cao đẳng, dự bị đại học.</p>

<p>Những trường ĐH như c&oacute; học bổng 50% : American University,University of Illinois, Adephil University,&nbsp; Auburn Univeristy, University of Kansas, University of Arizona, Univeristy of Central Florida&hellip;nổi tiếng.</p>

<h3><strong>4. Học bổng New Zealand</strong></h3>

<p>Học bổng của c&aacute;c trường thường tối đa đến 50%, c&ugrave;ng nhiều học bổng kh&aacute;c tại trường THPT, học viện, đại học&hellip;kh&aacute;c nhau.</p>

<p>V&iacute; dụ trường học viện c&ocirc;ng nghệ Nelson Marlborough (NMIT) cung cấp chương tr&igrave;nh học bổng tiếng Anh, cử nh&acirc;n với 100% gi&aacute; trị kh&oacute;a học trị gi&aacute; 6,000NZD hay 3,000 NZD.&rsquo;</p>

<p>Một số trường nổi tiếng với học bổng cao kh&aacute;c như: Học viện IPU (3,000 - 7,000NZD), Victoria University, University of Canterburry, Học viện Ara Canterbury, Taylor college, 12 trường THPT c&ocirc;ng lập ở Wellington, Le Cordon Bleu, Đại học Waikato&hellip;</p>

<h3><strong>5. Học bổng du học Canada</strong></h3>

<p>Du học Canada c&ograve;n nổi bật với du học diện visa miễn t&agrave;i ch&iacute;nh. Chi ph&iacute; học bổng du học từ 11.000-1600 CAD/năm. V&igrave; thế nơi đ&acirc;y cũng l&agrave; điểm đến được nhiều bạn lựa chọn n&ecirc;n t&iacute;nh cạnh tranh cũng tăng cao do đ&oacute; hiện ch&iacute;nh s&aacute;ch visa cũng c&oacute; thay đổi theo thời điểm.</p>

<h3><strong>6. Học bổng du học H&agrave; Lan</strong></h3>

<p>Những trường như Amsterdam University, Radboud University, TRotterdam University, he Hague University, Stenden University, Fontys University, HAN University, Inholland University, Saxion University, Tilburg University&hellip;c&oacute; nhiều suất học bổng cao tới 80-100%, học bổng 2.000-5.000 EUR.</p>

<h3><strong>7. Học bổng du học Thụy Sỹ</strong></h3>

<p>Gi&aacute; trị học bổng c&aacute;c trường thường từ 1.000-4.000 CHF (24-96 triệu) v&agrave; c&oacute; thể l&ecirc;n tới 1 tỷ 2 tại c&aacute;c trường ĐH lớn.</p>

<p>Th&ocirc;ng tin học bổng của c&aacute;c trường như BHMS, HTMI, SHMS, IHTTI, Cesar Ritz &hellip;d&agrave;nh cho cử nh&acirc;n, Thạc Sỹ h&agrave;ng năm.</p>

<h3><strong>9. Học bổng du học T&acirc;y Ban Nha, CH Sip</strong></h3>

<p>Du học tại hai nước n&agrave;y hiện đang c&oacute; chi ph&iacute; thấp hơn so với c&aacute;c nước kh&aacute;c n&ecirc;n được nhiều người hướng tới hiện nay. Những mức học bổng 50% của nhiều trường như BEBS, EUHT, tại hay UCAM, ESEI, BEBS, EUHT &hellip;đem lại lựa chọn đa dạng.</p>

<p>V&agrave; học bổng nhiều nước như Singpaore, c&aacute;c nước Đ&ocirc;ng &Acirc;u...kh&aacute;c.</p>

<h2><strong>II. Du học nước n&agrave;o tốt nhất?</strong></h2>

<p>Điều n&agrave;y sẽ l&agrave; trải nghiệm của mỗi người. Thường những trường được đ&aacute;nh gi&aacute; cao theo c&aacute;c bảng xếp hạng h&agrave;ng đầu thế giới. C&aacute;c bạn c&ugrave;ng tham khảo th&ecirc;m c&aacute;c bảng xếp hạng c&aacute;c trường ĐH tốt nhất thế giới ở link:&nbsp;<strong><a href="https://ielts-fighter.com/tin-tuc/Top-truong-Dai-hoc-hang-dau-the-gioi_mt1603994869.html" target="_blank" title="Top trường Đại học xếp hạng cao nhất thế giới">Top trường Đại học xếp hạng cao nhất thế giới</a></strong></p>

<p style="text-align:center"><img alt="học bổng du học và điều cần biết 2" src="https://static.ielts-fighter.com/uploads/2020/10/30/hoc-bong-du-hoc-va-cac-thong-tin-can-biet-2.jpg" style="height:492px; width:700px" /></p>

<p style="text-align:center"><em>Du học nước n&agrave;o cũng cần chuẩn bị kỹ</em></p>

<p>Tuy nhi&ecirc;n, để du học tại c&aacute;c nước, c&aacute;c bạn n&ecirc;n t&igrave;m hiểu n&ecirc;n văn h&oacute;a v&agrave; c&oacute; trang bị c&aacute;c kiến thức về văn h&oacute;a, x&atilde; hội, ch&iacute;nh trị...về đất nước đ&oacute; để h&ograve;a nhập được nhanh ch&oacute;ng hơn. C&oacute; nhiều sinh vi&ecirc;n, học sinh thường trải nghiệm c&aacute;c việc l&agrave;m th&ecirc;m hay t&igrave;m hiểu c&aacute;c anh chị đi trước để c&oacute; th&ecirc;m kinh nghiệm trong việc du học.</p>

<h2><strong>III. Điều kiện cần để xin học bổng</strong></h2>

<p>Mỗi trường y&ecirc;u cầu xin học bổng kh&aacute;c nhau n&ecirc;n c&aacute;c bạn cần t&igrave;m hiểu kỹ lưỡng để nộp đ&uacute;ng thời gian v&agrave; đủ hồ sơ cần thiết. Hồ sơ học bổng du học thường y&ecirc;u cầu chung về :</p>

<p>- Th&ocirc;ng tin c&aacute; nh&acirc;n, những điểm s&aacute;ng trong qu&aacute; tr&igrave;nh học tập, hoạt động ngoại kh&oacute;a, c&acirc;u chuyện ấn tượng.</p>

<p>-&nbsp;Bằng tiếng Anh. C&aacute;c chứng chỉ được chấp nhận phổ biến l&agrave; IELTS hoặc TOEFL iBT, GMAT, GRE, ACT, SAT.&nbsp;Hiện nay c&aacute;c chương tr&igrave;nh&nbsp;học bổng từ ch&iacute;nh phủ t&agrave;i trợ hay nhiều trường thường y&ecirc;u cầu IELTS 6.5 hoặc TOEFL iBT 79.&nbsp;</p>

<p>V&iacute; dụ như xin học bổng tại c&aacute;c trường đại học &Uacute;c&nbsp;y&ecirc;u cầu&nbsp;điểm trung b&igrave;nh đạt từ 8.0 trở l&ecirc;n, IELTS 6.5 hay du học tại Mỹ y&ecirc;u cầu&nbsp;TOEFL iBT 110.</p>

<p style="text-align:center"><img alt="học bổng du học và điều cần biết 3" src="https://static.ielts-fighter.com/uploads/2020/10/30/hoc-bong-du-hoc-va-cac-thong-tin-can-biet-3.jpg" style="height:842px; width:700px" /></p>

<p style="text-align:center"><em>IELTS rất quan trọng khi du học</em></p>

<p>-&nbsp;GPA - l&agrave; điểm trung b&igrave;nh t&iacute;ch lũy của học sinh, sinh vi&ecirc;n trong&nbsp;thời gian học tập. V&iacute; dụ với học sinh THPT th&igrave; điểm tổng kết từ 8.0-9.0 (tương đương A+), hay sinh vi&ecirc;n sau đại học th&igrave; điểm thường l&agrave; mức tr&ecirc;n 7.0 (B trở l&ecirc;n). Y&ecirc;u cầu điểm n&agrave;y ở trường n&agrave;o học bổng x&eacute;t khắt khe th&igrave; c&agrave;ng cao.</p>

<p style="text-align:center"><img alt="học bổng du học và điều cần biết 3" src="https://static.ielts-fighter.com/uploads/2020/10/30/hoc-bong-du-hoc-va-cac-thong-tin-can-biet-4.jpg" style="height:476px; width:700px" /></p>

<p style="text-align:center"><em>Tham khảo bảng quy đổi GPA</em></p>

<p>&nbsp;</p>

<p>Trong số những điều kiện cần để xin học bổng du học th&igrave; chứng chỉ tiếng Anh rất quan trọng. Hiện hầu như c&aacute;c trường đều chấp nhận chứng chỉ IELTS TOEFL, SAT&hellip;Trong đ&oacute; IELTS được chấp nhận rộng r&atilde;i v&agrave; nhiều bạn lựa chọn hơn cả.&nbsp;</p>

<p>Theo y&ecirc;u cầu chung của nhiều trường hiện nay, chứng chỉ từ 5.5, 6.5, 7.0, 7.5 l&agrave; cũng c&oacute; thể đủ điều kiện theo học tại trường. Thường y&ecirc;u cầu chung l&agrave; 6.5. Tuy nhi&ecirc;n, x&eacute;t học chương tr&igrave;nh THPT hay ĐH tại nước ngo&agrave;i, việc c&oacute; tiếng Anh tốt để theo kịp b&agrave;i giảng cũng như giao lưu văn h&oacute;a th&igrave; chứng chỉ 7.0 l&agrave; nhiều bạn hướng tới nhất.</p>

<p>7.0 IELTS đ&aacute;nh gi&aacute; khả năng sử dụng tiếng Anh tốt của th&iacute; sinh, theo cả 4 kỹ năng. Do đ&oacute;, việc tiếp nhận kiến thức v&agrave; giao tiếp, mở rộng, h&ograve;a nhập v&agrave;o thế giới xứ người th&ecirc;m thuận tiện cho c&aacute;c bạn du học sinh.</p>
', 'Kinh nghiệm');

INSERT INTO `News` (`NewsTitle`, `NewsImageAddress`, `NewsDescription`, `NewsContent`, `NewsCategory`)
VALUES
('Mẹo học tiếng Anh cho người mới bắt đầu', '/App/View/Source/Image/News/tips-for-beginner-learn-english.jpg', 'Mẹo học tiếng Anh này phù hợp với bạn, nếu bạn là người mới bắt đầu học tiếng Anh và không muốn rơi vào tình trạng không thể nói hoặc nghe tiếng Anh thì hãy tham khảo bảy mẹo học tiếng Anh cho người mới bắt đầu trong bài viết dưới đây và thử áp dụng với bản thân.',
 '<h3 dir="ltr"><strong>1. B&agrave;i tập học tiếng Anh&nbsp;hằng ng&agrave;y</strong></h3>

<p dir="ltr"><a href="https://giaovientienganh.edu.vn/"><em>Học ngoại ngữ</em></a>&nbsp;cũng giống như đi đến ph&ograve;ng tập thể dục, bạn phải luyện tập mỗi ng&agrave;y. Đối với&nbsp;<strong><em>tiếng Anh</em></strong>, thực h&agrave;nh l&agrave; một c&aacute;ch học ho&agrave;n hảo nhất.</p>

<p dir="ltr">Mẹo: Bạn h&atilde;y d&agrave;nh 15 ph&uacute;t mỗi ng&agrave;y để&nbsp;<strong><em>học tiếng Anh</em></strong>&nbsp;bằng c&aacute;ch nghe nhạc, podcast, đọc s&aacute;ch, xem phim ngắn tr&ecirc;n internet, chơi game tiếng Anh hay đơn giản l&agrave; gặp gỡ một v&agrave;i&nbsp;<a href="https://giaovientienganh.edu.vn/cung-cap-giao-vien-ban-ngu/"><em>người bản ngữ</em></a>.</p>

<h3 dir="ltr"><strong>2. Kh&ocirc;ng chỉ l&agrave; những từ vựng đơn lẻ</strong></h3>

<p dir="ltr">Học vi&ecirc;n mới bắt đầu&nbsp;<strong><em>học tiếng Anh</em></strong>&nbsp;sẽ cảm thấy thoải m&aacute;i khi nghe những từ vựng ri&ecirc;ng lẻ hay từ vựng được lặp đi lặp lại nhiều lần. Điều n&agrave;y kh&ocirc;ng c&oacute; vấn đề g&igrave; cho những ng&agrave;y đầu mới l&agrave;m quen v&agrave; tiếp x&uacute;c với tiếng Anh. Nhưng khi đ&atilde;&nbsp;<strong><em>tiếp x&uacute;c với tiếng Anh</em></strong>&nbsp;được v&agrave;i th&aacute;ng, bạn n&ecirc;n chuyển sang th&oacute;i quen học cả đoạn hoặc cả cụm từ để gi&uacute;p bạn dễ d&agrave;ng trong việc th&agrave;nh lập c&acirc;u v&agrave; khi n&oacute;i sẽ được tự nhi&ecirc;n v&agrave; tr&ocirc;i chảy hơn.</p>

<p dir="ltr">Mẹo: Bạn n&ecirc;n học c&aacute;c cụm collocations trong tiếng Anh. V&iacute; dụ: &ldquo;having breakfast&rdquo; (ăn s&aacute;ng) sẽ kh&aacute;c với &ldquo;making breakfast&rdquo; (chuẩn bị bữa s&aacute;ng).</p>

<h3 dir="ltr"><strong>3. Sự cố gắng</strong></h3>

<p dir="ltr">Mỗi khi bắt đầu một điều g&igrave; đ&oacute; mới mẻ, sẽ lu&ocirc;n c&oacute; một khả năng tiềm ẩn rằng bạn cũng c&oacute; thể bỏ cuộc giữa chừng. Ch&uacute;ng ta thường nghe c&acirc;u n&oacute;i: &ldquo;nếu bạn kh&ocirc;ng th&agrave;nh c&ocirc;ng lần đầu th&igrave; bạn vẫn c&ograve;n cơ hội thử lại lần thứ 2, thứ 3 v&agrave; nhiều lần nữa cho đến khi th&agrave;nh c&ocirc;ng thực sự mỉm cười&rdquo;.</p>

<p dir="ltr">Việc&nbsp;<strong><em>học một ngoại ngữ mới</em></strong>&nbsp;ho&agrave;n to&agrave;n so với tiếng mẹ đẻ kh&ocirc;ng phải l&agrave; điều dễ d&agrave;ng, với những cấu tr&uacute;c, từ vựng, nguy&ecirc;n tắc khiến bạn kh&ocirc;ng sao nhớ nổi nếu kh&ocirc;ng thường xuy&ecirc;n luyện tập. Nhưng việc luyện tập&nbsp;<em><strong>tiếng Anh cho người mới bắt đầu</strong></em>&nbsp;cũng đầy thử th&aacute;ch khi những lỗi sai cứ lặp đi lặp lại, rất dễ khiến người học nản l&ograve;ng v&agrave; muốn bỏ cuộc. Nếu một l&uacute;c n&agrave;o đ&oacute; bạn cảm thấy muốn bỏ cuộc tr&ecirc;n con đường chinh phục ngoại ngữ của m&igrave;nh th&igrave; h&atilde;y nhớ l&yacute; do tại sao bạn muốn bắt đầu. Điều n&agrave;y như một động lực gi&uacute;p bạn giữ lửa v&agrave; tiếp tục cố gắng.</p>

<p dir="ltr">Mẹo: Giữ một quyển sổ tay về những lỗi sai thường gặp của bạn trong qu&aacute; tr&igrave;nh&nbsp;<strong><em>học tiếng Anh</em></strong>. Ki&ecirc;n nhẫn sửa lỗi từng ch&uacute;t một, cho d&ugrave; bạn mắc lỗi h&agrave;ng chục hay h&agrave;ng trăm lần th&igrave; đừng vội bỏ cuộc, những lỗi sai kh&ocirc;ng thể đ&aacute;nh gi&aacute; khả năng tr&iacute; tuệ của bạn, m&agrave; ch&uacute;ng đang gi&uacute;p bạn r&egrave;n luyện v&agrave; ph&aacute;t triển những kỹ năng ngoại ngữ của m&igrave;nh.</p>

<h3 dir="ltr"><strong>4. D&aacute;n nh&atilde;n</strong></h3>

<p dir="ltr">C&oacute; một kỹ thuật đơn giản để ghi nhớ c&aacute;c từ vựng tiếng Anh mới đ&oacute; l&agrave; ghi nh&atilde;n. Phương ph&aacute;p học tiếng Anh n&agrave;y rất hữu &iacute;ch cho những người lớn tuổi học tiếng Anh. Họ c&oacute; thể ghi t&ecirc;n tiếng Anh của những đồ vật trong nh&agrave; v&agrave;o một mảnh giấy nhỏ v&agrave; d&aacute;n ch&uacute;ng l&ecirc;n những vật dụng tương ứng để đưa tiếng Anh v&agrave;o cuộc sống hằng ng&agrave;y.</p>

<p dir="ltr">Mẹo: H&atilde;y d&aacute;n nh&atilde;n mọi thứ m&agrave; bạn cần biết. N&ecirc;n sử dụng những mảnh giấy m&agrave;u để mỗi m&agrave;u sẽ đại diện cho một loại đồ vật. V&iacute; dụ: bạn c&oacute; thể sử dụng những mảnh ghi ch&uacute; m&agrave;u xanh l&aacute; c&acirc;y cho c&aacute;c thiết bị điện.</p>

<p dir="ltr">Ngo&agrave;i c&aacute;ch sử dụng những mảnh giấy để học từ vựng, c&aacute;c bạn cũng c&oacute; thể tải những&nbsp;ứng dụng&nbsp;<strong><em>học từ vựng tiếng Anh</em></strong>&nbsp;miễn ph&iacute; v&agrave;o c&aacute;c thiết bị di động để c&oacute; th&ecirc;m những trải nghiệm th&uacute; vị cho việc học.</p>

<h3 dir="ltr">&nbsp;</h3>

<h3 dir="ltr"><strong>5. Th&ecirc;m nhiều đối tượng v&agrave;o b&agrave;i học</strong></h3>

<p dir="ltr">Kh&ocirc;ng c&oacute; g&igrave; th&uacute; vị hơn l&agrave; bạn c&oacute; thể đem gia đ&igrave;nh v&agrave; bạn b&egrave; của m&igrave;nh v&agrave;o trong c&aacute;c b&agrave;i học tiếng Anh để tăng th&ecirc;m sự gần gũi v&agrave; th&uacute; vị. Tuy nhi&ecirc;n, bạn cũng cần lưu &yacute; về kh&iacute;a cạnh đạo đức khi mang những người th&acirc;n v&agrave;o b&agrave;i học.</p>

<p dir="ltr">Mẹo: Bạn c&oacute; thể quay video tiếng Anh về gia đ&igrave;nh hoặc bạn b&egrave;. C&oacute; thể thực hiện điều n&agrave;y mỗi tuần với nhiều chủ đề kh&aacute;c nhau. Sau một th&aacute;ng, chắc chắn bạn sẽ c&oacute; một số video th&uacute; vị v&agrave; khoảng thời gian vui vẻ khi xem lại c&ugrave;ng gia đ&igrave;nh hoặc bạn b&egrave; của m&igrave;nh.</p>

<blockquote><img alt="" src="https://pfn.vn/sites/default/files/anh_bai_viet/cach-hoan-thanh-muc-tieu-trong-ban-hang.png" style="height:522px; width:767px" />
<p>Bạn n&ecirc;n x&aacute;c định một mục ti&ecirc;u cụ thể để soi s&aacute;ng con đường học ngoại ngữ của m&igrave;nh.</p>
</blockquote>

<h3 dir="ltr"><strong>6. Đặt mục ti&ecirc;u</strong></h3>

<p dir="ltr">Bạn n&ecirc;n x&aacute;c định một mục ti&ecirc;u cụ thể v&agrave; tập trung v&agrave;o đ&oacute; khi bắt đầu học ngoại ngữ. H&atilde;y đặt&nbsp;<em><strong>mục ti&ecirc;u để học tiếng Anh</strong></em>&nbsp;xoay quanh lĩnh vực m&agrave; bạn đang quan t&acirc;m. Điều n&agrave;y sẽ rất hữu &iacute;ch cho việc học của bạn. V&iacute; dụ: nếu bạn muốn đi mua sắm quần &aacute;o ở một quốc gia n&oacute;i tiếng Anh, bạn sẽ phải học những cụm từ li&ecirc;n quan như: &ldquo;Where is the changing room?&rdquo; hoặc &ldquo;Can I pay by credit card?&rdquo;.</p>

<p dir="ltr">Mẹo: H&atilde;y viết những g&igrave; bạn muốn học trong một tuần v&agrave;o quyển sổ tay. V&agrave;o cuối tuần, kiểm tra xem bạn đ&atilde; đạt được những mục ti&ecirc;u tiếng Anh n&agrave;o v&agrave; điều g&igrave; đ&atilde; cản trở cũng như hỗ trợ cho việc học của bạn. Bằng c&aacute;ch trả lời những c&acirc;u hỏi n&agrave;y, bạn sẽ cải thiện rất nhiều cho qu&aacute; tr&igrave;nh học của m&igrave;nh.</p>

<h3 dir="ltr"><strong>7. Khoảng thời gian vui vẻ</strong></h3>

<p dir="ltr">Thời gian đầu khi mới&nbsp;<strong><em>học tiếng Anh</em></strong>&nbsp;thực sự kh&ocirc;ng dễ d&agrave;ng ch&uacute;t n&agrave;o. Bạn sẽ cảm thấy kh&aacute; thất vọng v&agrave; đ&acirc;y l&agrave; khoảng thời gian dễ khiến người học từ bỏ v&igrave; nản l&ograve;ng. V&igrave; vậy, bạn phải tạo ra khoảnh khắc học tập vui vẻ để th&uacute;c đẩy những cảm x&uacute;c t&iacute;ch cực. Khi những cảm x&uacute;c n&agrave;y được li&ecirc;n kết với qu&aacute; tr&igrave;nh học, bạn sẽ thấy những&nbsp;<em>kỹ năng tiếng Anh</em>&nbsp;của m&igrave;nh được cải thiện một c&aacute;ch đ&aacute;ng kể.</p>

<p dir="ltr">Mẹo: nếu bạn mắc lỗi, chỉ cần &ldquo;cười v&agrave;o ch&uacute;ng&rdquo;. Điều quan trọng trong qu&aacute; tr&igrave;nh học tiếng Anh l&agrave; vui vẻ, thoải m&aacute;i v&agrave; ki&ecirc;n tr&igrave;.</p>

<p dir="ltr">Với mẹo học tiếng anh cho người mới th&igrave; sẽ dễ d&agrave;ng hơn, rất tuyệt vời nếu bạn c&oacute; thể thể hiện bản th&acirc;n bằng tiếng Anh, quả l&agrave; một thử th&aacute;ch đầy th&uacute; vị phải kh&ocirc;ng n&agrave;o! Ch&uacute;c c&aacute;c bạn th&agrave;nh c&ocirc;ng.</p>
', 'Mẹo vặt');

INSERT INTO `News` (`NewsTitle`, `NewsImageAddress`, `NewsDescription`, `NewsContent`, `NewsCategory`)
VALUES
('IELTS là gì ? Lệ phí thi bao nhiêu ? Thi ở đâu ?', '/App/View/Source/Image/News/ielts-infor.jpg', 'Chứng chỉ IELTS được xem là tấm giấy thông hành mở ra cơ hội sự nghiệp và học tập mang tính quốc tế. Cùng Sunflower Center hiểu rõ về Cấu Trúc Đề Thi IELTS để chuẩn bị thật tốt cho kỳ thi IELTS của bạn nhé.',
 '<h1>Giới thiệu về k&igrave; thi IELTS</h1>

<p><strong>IELTS</strong>&nbsp;(viết tắt từ International English Language Testing System) l&agrave; một k&igrave; thi Anh ngữ chuẩn c&agrave;ng ng&agrave;y c&agrave;ng được thừa nhận rộng r&atilde;i tr&ecirc;n thế giới. B&agrave;i kiểm&nbsp;<strong>tra IELTS</strong>&nbsp;được sử dụng để đ&aacute;nh gi&aacute; tr&igrave;nh độ tiếng Anh của những người mong muốn đi học hay l&agrave;m việc trong một m&ocirc;i trường n&oacute;i tiếng Anh. Phần lớn ứng vi&ecirc;n l&agrave; sinh vi&ecirc;n muốn tiếp tục được đ&agrave;o tạo ở nước ngo&agrave;i v&agrave; những người t&igrave;m c&aacute;ch khởi nghiệp ở nước ngo&agrave;i. Hiện nay c&oacute; hơn 10&nbsp;000 tổ chức, cả c&ocirc;ng v&agrave; tư, chấp nhận chứng chỉ IELTS.</p>

<p>Mỗi năm c&oacute; đến hơn 3 triệu th&iacute; sinh tham dự k&igrave; thi IELTS, theo nguồn tin từ website của Hội đồng Anh (British Council). Con số đ&oacute; n&oacute;i l&ecirc;n một điều l&agrave; k&igrave; thi IELTS l&agrave; một trong những k&igrave; kiểm tra đ&aacute;nh gi&aacute; ng&ocirc;n ngữ phổ biến, nổi tiếng nhất tr&ecirc;n thế giới.</p>

<p style="text-align:center"><img alt="IELTS LÀ GÌ? LỆ PHÍ THI BAO NHIÊU? THI Ở Đ U?" src="https://global-exam.com/blog/wp-content/uploads/2020/06/ielts-viet-test-1-300x200.jpg" style="height:333px; width:500px" /></p>

<p>C&oacute;&nbsp;<strong>2 dạng IELTS:</strong></p>

<ol>
	<li><strong>IELTS Học thuật/ IELTS Academic</strong>&nbsp;d&agrave;nh cho những sinh vi&ecirc;n muốn theo học đại học v&agrave; sau đại học trong c&aacute;c cơ sở đ&agrave;o tạo ở nước ngo&agrave;i.</li>
	<li><strong><em>IETLS Tổng qu&aacute;t/ IELTS General Training</em></strong>&nbsp;cho ph&eacute;p những người muốn l&agrave;m việc ở nước ngo&agrave;i được chứng thực tr&igrave;nh độ Anh ngữ của họ.</li>
</ol>

<p>D&ugrave; bạn thi theo dạng IELTS n&agrave;o th&igrave; một lời khuy&ecirc;n quan trọng ch&uacute;ng t&ocirc;i d&agrave;nh cho bạn l&agrave; cần&nbsp;<strong>phải lập kế hoạch chuẩn bị &ocirc;n thi&nbsp;</strong>để kh&ocirc;ng bị bất ngờ v&agrave;o ng&agrave;y đi thi.</p>

<p>B&agrave;i kiểm tra IELTS d&ugrave;ng để đ&aacute;nh gi&aacute;<strong>&nbsp;4 kỹ năng ng&ocirc;n ngữ:</strong></p>

<ul>
	<li><strong>Listening</strong>&nbsp;(kỹ năng Nghe) bao gồm 40 c&acirc;u hỏi kh&aacute;c nhau dưới dạng c&aacute;c đề b&agrave;i kh&aacute;c nhau: c&acirc;u hỏi nhiều lựa chọn, điền v&agrave;o chỗ trống, nối c&acirc;u hỏi với c&acirc;u trả lời, ho&agrave;n thiện sơ đồ&hellip;</li>
	<li><strong>Reading</strong>&nbsp;(kỹ năng Đọc) cũng bao gồm 40 c&acirc;u hỏi kh&aacute;c nhau dựa tr&ecirc;n c&aacute;c đề b&agrave;i ở đủ c&aacute;c dạng thức kh&aacute;c nhau: c&acirc;u hỏi nhiều lựa chọn, điền v&agrave;o chỗ trống, nối c&acirc;u hỏi với c&acirc;u trả lời, ho&agrave;n thiện sơ đồ&hellip;</li>
	<li><strong>Writing</strong>&nbsp;(kỹ năng Viết) bao gồm 2 b&agrave;i viết luận: b&agrave;i thứ nhất c&oacute; &iacute;t nhất 150 từ v&agrave; b&agrave;i thứ hai c&oacute; &iacute;t nhất 250 từ.</li>
	<li><strong>Speaking</strong>&nbsp;(kỹ năng N&oacute;i) diễn ra dưới h&igrave;nh thức một cuộc phỏng vấn c&aacute; nh&acirc;n trực diện với một vị gi&aacute;m khảo IELTS, bao gồm 3 phần: gi&aacute;m khảo đặt những c&acirc;u hỏi li&ecirc;n quan đến cuộc sống c&aacute; nh&acirc;n của bạn, gi&aacute;m khảo đề nghị bạn n&oacute;i về một chủ đề được r&uacute;t thăm một c&aacute;ch ngẫu nhi&ecirc;n v&agrave; sau đ&oacute; sẽ thiết lập một cuộc trao đổi mang t&iacute;nh chất kh&aacute;i qu&aacute;t hơn thường về chủ đề đ&atilde; được bốc thăm.</li>
</ul>

<h2><strong>Ph&acirc;n lượng thời gian</strong>&nbsp;(như nhau đối với cả hai dạng thi IELTS)</h2>

<ul>
	<li><strong>Listening</strong>&nbsp;k&eacute;o d&agrave;i 40 ph&uacute;t</li>
</ul>

<p>&deg; 30 ph&uacute;t nghe</p>

<p>&deg; 10 ph&uacute;t để điền c&aacute;c c&acirc;u trả lời l&ecirc;n phiếu trả lời</p>

<ul>
	<li><strong>Reading</strong>&nbsp;c&oacute; thời lượng 60 ph&uacute;t</li>
</ul>

<p>&deg; trung b&igrave;nh 20 ph&uacute;t cho mỗi văn bản</p>

<ul>
	<li><strong>Writing</strong>&nbsp;k&eacute;o d&agrave;i 60 ph&uacute;t</li>
</ul>

<p>&deg; 20 ph&uacute;t cho b&agrave;i luận thứ nhất chứa &iacute;t nhất 150 từ</p>

<p>&deg; 40 ph&uacute;t cho b&agrave;i luận thứ hai chứa &iacute;t nhất 250 từ</p>

<ul>
	<li><strong>Speaking</strong>&nbsp;c&oacute; thời lượng dao động từ 10 đến 15 ph&uacute;t</li>
</ul>

<p>&deg; từ 2 đến 3 ph&uacute;t cho phần phỏng vấn thứ nhất (c&acirc;u hỏi li&ecirc;n quan đến c&aacute; nh&acirc;n th&iacute; sinh)</p>

<p>&deg; từ 4 đến 5 ph&uacute;t cho phần phỏng vấn thứ hai (c&acirc;u hỏi li&ecirc;n quan đến chủ đề bốc thăm)</p>

<p>&deg; từ 4 đến 7 ph&uacute;t cho phần phỏng vấn thứ ba (c&acirc;u hỏi mang t&iacute;nh kh&aacute;i qu&aacute;t hơn)</p>

<p>Tổng cộng thời lượng của c&aacute;c b&agrave;i kiểm tra IELTS dao động&nbsp;<strong>từ 2 giờ 50 ph&uacute;t đến 2 giờ 55 ph&uacute;t.</strong></p>

<h2>C&aacute;c chủ đề của IELTS</h2>

<p>Bởi v&igrave; b&agrave;i kiểm tra IELTS c&oacute; mục đ&iacute;ch đ&aacute;nh gi&aacute; tr&igrave;nh độ ng&ocirc;n ngữ chung của th&iacute; sinh n&ecirc;n&nbsp;<strong>tất cả c&aacute;c chủ đề mang t&iacute;nh thời sự c&oacute; thể sẽ được đề cập</strong>&nbsp;trong cả 4 phần thi.</p>

<ul>
	<li><strong>To&agrave;n cảnh x&atilde; hội hiện đại</strong>&nbsp;(to&agrave;n cầu h&oacute;a, những tiến bộ khoa học kĩ thuật, c&ocirc;ng nghệ mới, y tế sức khỏe&hellip;)</li>
	<li><strong>Cuộc sống thường nhật</strong>&nbsp;(giao th&ocirc;ng, đồ ăn thức uống, giải tr&iacute;&hellip;)</li>
	<li><strong>Gi&aacute;o dục</strong>&nbsp;(việc học tập, việc học ngoại ngữ, chương tr&igrave;nh đại học&hellip;)</li>
	<li><strong>M&ocirc;i trường</strong>&nbsp;(hệ sinh th&aacute;i, biến đổi kh&iacute; hậu, c&aacute;c lo&agrave;i c&oacute; nguy cơ tuyệt chủng&hellip;)</li>
	<li><strong>C&ocirc;ng việc</strong>&nbsp;(thất nghiệp, nghỉ hưu, t&aacute;c động của ch&iacute;nh phủ&hellip;)</li>
</ul>

<p>Lưu &yacute; rằng b&agrave;i kiểm tra IELTS Reading của dạng IELTS Tổng qu&aacute;t thường đưa ra c&aacute;c văn bản tập trung hơn về chủ đề c&ocirc;ng việc, nghề nghiệp trong khi đ&oacute; b&agrave;i kiểm tra IELTS Reading của dạng IELTS Học thuật c&oacute; thể đề cập đến bất k&igrave; chủ đề n&agrave;o được n&ecirc;u ở tr&ecirc;n.</p>

<h2>Thi IELTS ở đ&acirc;u?</h2>

<p>Bạn chỉ được ph&eacute;p thi IELTS ở c&aacute;c trung t&acirc;m được Hội đồng Anh (British Council) ủy nhiệm. Ở Việt Nam, k&igrave; thi IELTS được tổ chức tại H&agrave; Nội, Th&agrave;nh phố Hồ Ch&iacute; Minh v&agrave; hơn 20 tỉnh, th&agrave;nh phố tr&ecirc;n cả nước. Th&iacute; sinh c&oacute; thể đăng k&iacute; thi trực tiếp tại Văn ph&ograve;ng Hội đồng Anh hoặc đăng k&iacute; th&ocirc;ng qua hệ thống c&aacute;c đối t&aacute;c ch&iacute;nh thức tổ chức thi IELTS của Hội đồng Anh.</p>

<p>Ở H&agrave; Nội v&agrave; Th&agrave;nh phố Hồ Ch&iacute; Minh, bạn c&oacute; thể đăng k&iacute; thi IELTS trực tiếp với Hội đồng Anh, với lịch thi l&ecirc;n đến 4 ng&agrave;y mỗi th&aacute;ng (từ th&aacute;ng 1 đến th&aacute;ng 6). Ngo&agrave;i ra, tại H&agrave; Nội, bạn c&oacute; thể đăng k&iacute; thi với c&aacute;c đối t&aacute;c của Hội đồng Anh như: Học viện Anh ngữ 5 sao Atlantic Five-Star English, GLN English Center, Đại học Kinh tế Quốc d&acirc;n. Tại Th&agrave;nh phố Hồ Ch&iacute; Minh, bạn c&oacute; thể đăng k&iacute; thi IELTS với đối t&aacute;c của Hội đồng Anh l&agrave; Trường Cao đẳng Quốc tế Kent. C&aacute;c tỉnh, th&agrave;nh phố c&oacute; c&aacute;c trung t&acirc;m được Hội đồng Anh ủy nhiệm tổ chức thi IELTS l&agrave;: Hải Ph&ograve;ng, Hạ Long (Quảng Ninh), Bắc Giang, Ph&uacute; Thọ, Thanh H&oacute;a, Vinh (Nghệ An), Huế, Đ&agrave; Nẵng, Quy Nhơn (B&igrave;nh Định), Quảng Ng&atilde;i, Tuy H&ograve;a (Ph&uacute; Y&ecirc;n), Quảng Trị, Bu&ocirc;n Ma Thuột (Đắc Lắc), Pleiku (Gia Lai), Nha Trang, Đ&agrave; Lạt, Vũng T&agrave;u, Bi&ecirc;n H&ograve;a (Đồng Nai), Đồng Th&aacute;p, Cần Thơ, B&igrave;nh Dương, Tr&agrave; Vinh, Vĩnh Long.</p>

<p>Ch&uacute;ng t&ocirc;i khuy&ecirc;n bạn n&ecirc;n đăng k&iacute; thi &iacute;t nhất trước 2 th&aacute;ng.</p>

<p><strong>Lệ ph&iacute; thi</strong>&nbsp;cho mỗi dạng IELTS (Học thuật v&agrave; Tổng qu&aacute;t) l&agrave; 4&nbsp;750&nbsp;000 VNĐ.</p>

<h2>V&igrave; sao n&ecirc;n thi lấy chứng chỉ IELTS?</h2>

<p>Chứng chỉ IELTS gi&uacute;p bạn mở v&ocirc; số c&aacute;nh cửa cả về mặt học thuật v&agrave; c&ocirc;ng việc. Đ&acirc;y l&agrave; một chứng chỉ ng&ocirc;n ngữ nổi tiếng được c&ocirc;ng nhận tr&ecirc;n to&agrave;n thế giới, trong đ&oacute; c&oacute; &Uacute;c, Canada, Niu Di-l&acirc;n, Anh v&agrave; Mỹ.</p>

<p>&nbsp;</p>
', 'Tin tức');