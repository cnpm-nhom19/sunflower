<?php
    require_once('App/Model/ScheduleModel.php');

    class ScheduleController extends BaseController{
        public function index(){
            if(isset($_SESSION["fullname"])){

                $studentId = $_SESSION["id"];
                $studentFullname = $_SESSION["fullname"];

                $model = new ScheduleModel();
                $result = $model -> getStudentCourseById($studentId);
                $studentCourseArray = $result['data'];

                $this->renderView('Schedule.html', ['fullname' => $studentFullname, "studentCourseArray" => $studentCourseArray]);
            }
            else{
                $this->renderView('Schedule.html', []);
            }
        }
    }
?>