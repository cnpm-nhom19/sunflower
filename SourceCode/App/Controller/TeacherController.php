<?php
    require_once('App/Model/TeacherModel.php');

    class TeacherController extends BaseController{

        public function index(){
            $this->renderView('HomeTeacher.html', []);
        }

        public function uploadImage($targetDir, $nameInput){
            $targetFile = $targetDir . time() . basename($_FILES[$nameInput]['name']);
            $imageFileType = strtolower(pathinfo($targetFile, PATHINFO_EXTENSION));

            if ($imageFileType != "jpg" && $imageFileType != "jpeg" && $imageFileType != "png"){
                echo "File không đúng định dạnh cho phép";
                return "";
            }
            move_uploaded_file($_FILES[$nameInput]["tmp_name"], $targetFile);
            return $targetFile;
        }

        public function addClass(){
            $this->renderView('AddClass.html', []);
        }

        public function getTeacherSchedule(){

            if(isset($_SESSION['username'])){
                $username = $_SESSION['username'];
                $model = new TeacherModel();
                $result = $model -> getTeacherCourseByUsername($username);
                $teacherCourseArray = $result['data'];
                
                $this->renderView('TeacherSchedule.html', ["teacherCoureArray" => $teacherCourseArray]);
            }
            else{
                $this->renderView('TeacherSchedule.html', []);
            }
        }

        public function getTeacherInfor(){
            $model = new TeacherModel();
            $result = $model->getById($_SESSION['id']);

            $teacher = $result['data'][0];

            $this->renderView('TeacherInfor.html', ["teacher" => $teacher]);
        }
        //GET editInforTeacher
        public function editInfoTeacher(){
            $model = new TeacherModel();
            $result = $model->getById($_SESSION['id']);

            $teacher = $result['data'][0];

            $this->renderView('EditInfoTeacher.html', ["teacher" => $teacher]);
        }

        //POST editInforTeacher
        public function editInfoTeacherPost(){
            if(isset($_POST['fullname'])){
                $username = $_POST['username'];
                $fullname = $_POST['fullname'];
                $email = $_POST['email'];
                $language = $_POST['language'];
                $phone = $_POST['phone'];
                $degree = $_POST['degree'];

                if(isset($_FILES['avatar-image'])){
                    if( $_FILES['avatar-image']['name'] ){
                        $targetDir = "App/View/Source/Image/Teacher/";
                        $nameInput = "avatar-image";
                        $avatarURL = '/' . $this->uploadImage($targetDir, $nameInput);
                    }
                    else{
                        $avatarURL = '';
                    }
                }
                else{
                    $avatarURL = '';
                }
                $model = new TeacherModel();
                $result = $model -> updateTeacherInformationByUsername($username, $email, $fullname, $language, $phone, $avatarURL, $degree);

                $_SESSION["username"] = $username;
                $_SESSION["fullname"] = $fullname;
                $_SESSION["email"] = $email;
            }
            $this->editInfoTeacher();
        }

        public function getTeacherCourse(){
            $model = new TeacherModel();
            $result = $model->getTeacherCourseByUsername($_SESSION['username']);

            $teacherCourseArray = $result['data'];

            $this->renderView('CreatedClass.html', ["teacherCourseArray" => $teacherCourseArray]);
        }

        public function logout(){
            session_start();
            session_destroy();

            header('Location: /home/index');
        }

    }
?>